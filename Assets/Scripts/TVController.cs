using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TVController : Singleton<TVController>
{
    [SerializeField]
    private VideoPlayer videoPlayer;
    [SerializeField]
    private Image PointLight;
    [SerializeField]
    private Material tvMAterial;

    float timer = 0.0f;
    public bool played = false;

    private void Awake()
    {
        tvMAterial.color = Color.grey;
    }

    private void Update()
    {
        if (GameRunnerStateMachine.Instance.CurrentSate.Type == StateType.Maidan)
        {
            if (videoPlayer.enabled)
            {
                PointLight.color = Color.green;
                tvMAterial.color = Color.white;
                if (played == false)
                {
                    TurnOffTv().Forget();
                    played = true;
                }
            }
            else
            {
                timer += Time.deltaTime;
                tvMAterial.color = Color.grey;
                if (timer < 0.25f)
                {
                    PointLight.color = Color.red;
                }
                else if (timer < 0.5f)
                {
                    PointLight.color = Color.black;
                }
                else
                {
                    AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.TVBlink, transform.position);
                    timer = 0;
                }
            }
        }
    }

    public void TurnOnTv()
    {
        videoPlayer.enabled = true;
    }

    public async UniTask TurnOffTv()
    {
        await DialogSystem.Instance.DisplayTextCharacterAwaitable("TV: Protests against Yanukovych decision to withdraw Ukraine accession to EU began",0, 1.5f);
        await UniTask.Delay(3000, true);
        DoorsController.Instance.Locked = false;
        await DialogSystem.Instance.DisplayTextCharacterAwaitable("WOW, I have to go to Kiyev right now!", 0, 1.5f);
    }
}
