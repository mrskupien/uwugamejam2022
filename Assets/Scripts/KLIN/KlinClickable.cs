using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KlinClickable : Clickable
{
	[SerializeField] private LoreScriptable normalOvenLore;
	[SerializeField] private LoreScriptable burntIgredientLore;
	[SerializeField] private LoreScriptable breadYeetedIntoTheOven;
	[SerializeField] private PlacementArea bakeArea;

	public override void OnClick()
	{
		BakeTheBread bread = PlayerManager.Instance.ItemsPickupableController.ObjectInHands as BakeTheBread;
		if(bread != null)
		{
			DialogSystem.Instance.DisplayTextCharacterAwaitable(breadYeetedIntoTheOven.Lore, endPauseSeconds: 1.5f);
			PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, bakeArea);
			bakeArea.OnPickablePlaced += StartBakingBread;
		}
		else if(PlayerManager.Instance.ItemsPickupableController.ObjectInHands)
		{
			PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, bakeArea);
			bakeArea.OnPickablePlaced += BurnIgredient;
		}
		else
		{
			DialogSystem.Instance.DisplayTextCharacterAwaitable(normalOvenLore.Lore, endPauseSeconds: 1.5f);
		}
	}
	private void StartBakingBread() 
	{
		bakeArea.OnPickablePlaced -= StartBakingBread;
		BakeTheBread.StartBakingBread?.Invoke(); 
	}
	private void BurnIgredient()
	{
		bakeArea.OnPickablePlaced -= BurnIgredient;
		Destroy(bakeArea.PickupableInPlacementArea.gameObject);
		DialogSystem.Instance.DisplayTextCharacterAwaitable(burntIgredientLore.Lore, endPauseSeconds: 1.5f);
	}
}
