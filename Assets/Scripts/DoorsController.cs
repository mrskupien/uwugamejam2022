using Cysharp.Threading.Tasks;
using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsController : Singleton<DoorsController>
{
    [SerializeField]
    private List<StateType> enums;
    [SerializeField]
    private TriggerHandler left;
    [SerializeField]
    private TriggerHandler right;
    [SerializeField]
    private DoorClickable clickablel;
    [SerializeField]
    private DoorClickable clickablel2;
    [SerializeField]
    private DoorClickable clickablel3;
    [SerializeField]
    private GameObject rotation;
    [SerializeField]
    private Vector3 startRotation;
    [SerializeField]
    private Vector3 endRotation1;
    [SerializeField]
    private Vector3 endRotation2;
    [SerializeField]
    private LoreScriptable doorOpenedLore;

    private bool locked = true;
    public bool Locked
    {
        get => locked;
        set
        {
            locked = value;
            if(!locked)
			{
                AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.UnlockDoor, transform.position);
                //DialogSystem.Instance.DisplayTextCharacterAwaitable(doorOpenedLore.Lore, endPauseSeconds: 0.5f).Forget();
            }
        }
    }


    int i = 0;
    private void OnEnable()
    {
        clickablel.Click += OpenDoors;
        if (clickablel2 != null)
        {
            clickablel2.Click += OpenDoors;
        }
        if (clickablel3)
        {
            clickablel3.Click += OpenDoors;
        }
    }


    public void Update()
    {
        if (GameRunnerStateMachine.Instance.CurrentSate.Type == StateType.Medival)
        {
            clickablel.gameObject.SetActive(true);
            clickablel3.gameObject.SetActive(false);
        }
        else
        {
            clickablel.gameObject.SetActive(false);
            clickablel3.gameObject.SetActive(true);
        }


        if (Input.GetKeyDown(KeyCode.K))
        {
            Locked = false;
        }


    }

    public void OpenDoors()
    {
        if (!Locked)
        {
            StartCoroutine(OpenDoorsIen());
            AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.OpenDoor, transform.position);
            Locked = true;
        }
        else
        {
            AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.LockedDoor, transform.position);
        }
    }

    private IEnumerator OpenDoorsIen()
    {
        yield return StartCoroutine(Utility.Lerp.SmoothFloat(
        0, 1, 1.0f,
        (newFloat) => rotation.transform.rotation = Quaternion.Euler(Vector3.Lerp(startRotation, Vector3.Dot(FirstPersonController.Instance.transform.forward, transform.right) > 0 ? endRotation2 : endRotation1, newFloat)), () => StartCoroutine(WaitToCross())
        ));
    }

    private IEnumerator WaitToCross()
    {
        if (enums[i % enums.Count] == StateType.Final)
        {
            GameRunnerStateMachine.Instance.SwitchState(enums[i % enums.Count], false);
            i++;
            yield return new WaitForSeconds(4.0f);
            bool test3 = (rotation.transform.rotation.eulerAngles - endRotation1).magnitude % 360.0f == 0.0f;
            yield return StartCoroutine(Utility.Lerp.SmoothFloat(
            0, 1, 1.0f,
            (newFloat) => rotation.transform.rotation = Quaternion.Euler(Vector3.Lerp(test3 ? endRotation1 : endRotation2, startRotation, newFloat)), null
                ));
            yield break;
        }
    Start:
        bool test = (rotation.transform.rotation.eulerAngles - endRotation1).magnitude % 360.0f == 0.0f;
        var temp = test ? left : right;
        var temp2 = !test ? left : right;
        yield return new WaitUntil(() => temp.Triggered == true);
        yield return new WaitUntil(() => temp.Triggered == false);
        if (temp2.Triggered == true)
            goto Start;
        yield return StartCoroutine(Utility.Lerp.SmoothFloat(
        0, 1, 1.0f,
        (newFloat) => rotation.transform.rotation = Quaternion.Euler(Vector3.Lerp(test ? endRotation1 : endRotation2, startRotation, newFloat)), null
        ));
        GameRunnerStateMachine.Instance.SwitchState(enums[i % enums.Count], false);
        i++;
    }
}
