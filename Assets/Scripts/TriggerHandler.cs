using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHandler : MonoBehaviour
{
    public Action<Collider> OnTriggerEnterAction;
    public Action<Collider> OnTriggerExitAction;
    public bool Triggered = false;

    public void OnTriggerEnter(Collider other)
    {
        Triggered = true;
        OnTriggerEnterAction?.Invoke( other);
    }

    public void OnTriggerExit(Collider other)
    {
        Triggered = false;
        OnTriggerExitAction?.Invoke(other);
    }
}
