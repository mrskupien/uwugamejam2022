using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CastingType
{
    None = 0,
    FromCrosshair = 1,
    FromMouse = 2
}

public class RaycastManager : Singleton<RaycastManager>
{
    public Action<Interactable> OnRayEnterEvent;
    public Action<Interactable> OnRayStayEvent;
    public Action<Interactable> OnRayExitEvent;

    public Action<CastingType> OnChangeRaycastMode;

    [SerializeField] int interactableLayer;
    [SerializeField] float raycastLength;
    private int layerMask;
    private CastingType currentCastingType;
    private RaycastHit hit;
    private Camera mainCamera;
    public bool IsActive { get; set; }

    private Interactable selectedInteractable;
    private Interactable previousInteractable;

    public Interactable SelectedInteractable => selectedInteractable;

    public CastingType CurrentCastingType  => currentCastingType; 
    private void Awake()
    {
        layerMask = 1 << interactableLayer;
        OnChangeRaycastMode += (CastingType castingType) =>
        {
            currentCastingType = castingType;
        };
        OnChangeRaycastMode(CastingType.FromCrosshair);
        IsActive = true;
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (IsActive)
        {
            PerformRaycast();
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (selectedInteractable != null)
            {
                if (selectedInteractable is Clickable clickable)
                {
                    clickable.OnClick();
                }
                else if (selectedInteractable is Holdable holdable)
                {
                    holdable.OnHold();
                }

            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (RadioController.Instance.Focused)
            {
                RadioController.Instance.OnPutItemBackOnTable(PlayerManager.Instance.ItemsPickupableController.ObjectInHands);
            }
            else
            {
                PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true);
            }    
        }
    }

    void PerformRaycast()
    {
        switch (currentCastingType)
        {
            case CastingType.FromCrosshair:
                PerformRaycastFromCrosshair();
                break;
            case CastingType.FromMouse:
                PerformRaycastFromMousePos();
                break;
            default:
                break;
        }
    }

    private void PerformRaycastFromMousePos()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, raycastLength, layerMask))
        {
            SetInteractable();
        }
        else
        {
            if (selectedInteractable != null)
            {
                selectedInteractable.OnRayExit();
                OnRayExitEvent?.Invoke(selectedInteractable);
                selectedInteractable = null;
            }
        }
    }

    private void PerformRaycastFromCrosshair()
    {
        Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, raycastLength, layerMask);
        SetInteractable();
    }

    void SetInteractable()
    {
        if (hit.collider == null)
        {
            if (selectedInteractable != null)
            {
                selectedInteractable.OnRayExit();
                OnRayExitEvent?.Invoke(selectedInteractable);
                previousInteractable = selectedInteractable;
                selectedInteractable = null;
            }
            return;
        }

        Interactable newInteractable = hit.collider.gameObject.GetComponent<Interactable>();

        if (newInteractable != null)
        {
            selectedInteractable = newInteractable;
            if (newInteractable == previousInteractable)
            {
                previousInteractable.OnRayStay();
                OnRayStayEvent?.Invoke(previousInteractable);
            }
            else
            {
                previousInteractable?.OnRayExit();
                OnRayExitEvent?.Invoke(previousInteractable);

                newInteractable?.OnRayEnter();
                OnRayEnterEvent?.Invoke(newInteractable);
                previousInteractable = previousInteractable == null ? newInteractable : selectedInteractable;
            }
        }
        //else
        //{
        //    if (previousInteractable != null)
        //    {
        //        previousInteractable?.OnRayExit();
        //        OnRayExitEvent?.Invoke(selectedInteractable);
        //        previousInteractable = null;
        //    }
        //}
        


        //if (newSelected != null)
        //{
        //    if (newSelected != previousInteractable && previousInteractable != null)
        //    {
        //        previousInteractable = selectedInteractable;
        //        newSelected.OnRayEnter();
        //        OnRayEnterEvent?.Invoke(newSelected);
        //        selectedInteractable = newSelected;
        //    }
        //    else
        //    {
        //        selectedInteractable = newSelected;
        //        selectedInteractable.OnRayStay();
        //        OnRayStayEvent?.Invoke(selectedInteractable);
        //    }
        //}
    }
}
