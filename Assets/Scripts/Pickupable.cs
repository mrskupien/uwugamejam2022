﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class Pickupable : Clickable
{
    public static int PickupableLayer = 7;

    [SerializeField] private Rigidbody objRigidbody;
    private PlacementArea currentPlacementArea;
    private Collider pickupableCollider;
    public Action<Pickupable> OnPutDownEvent;
    public Action<Pickupable> OnPickedUpEvent;
    public Rigidbody ObjRigidbody => objRigidbody;
    public Collider ObjCollider => pickupableCollider ??= GetComponent<Collider>();

    public PlacementArea CurrentPlacementArea => currentPlacementArea; 

    public override void OnClick()
    {
        PlayerManager.Instance.ItemsPickupableController.HandlePickingUp(this, true);
    }

    public virtual void OnPickUp(PlacementArea occupiedPlacementArea, bool shouldLerp = false, bool isBread = false)
    {
        occupiedPlacementArea.SetObjectOnPlacementTransform(this, shouldLerp,true, isBread);
        occupiedPlacementArea.IsOccupied = true;
        ResetPlacementArea();
        OnPickedUpEvent?.Invoke(this);
    }

    public virtual void OnPutDown(PlacementArea placementAreaToOccupy, bool shouldLerp = false, bool isBread = false)
    {
        placementAreaToOccupy.SetObjectOnPlacementTransform(this, shouldLerp, false, isBread);
        ResetPlacementArea();
        placementAreaToOccupy.IsOccupied = true;
        currentPlacementArea = placementAreaToOccupy;
        OnPutDownEvent?.Invoke(this);
    }

    public void ResetPlacementArea()
    {
        if (currentPlacementArea != null)
        {
            currentPlacementArea.IsOccupied = false;
            currentPlacementArea = null;
        }
    }
}
