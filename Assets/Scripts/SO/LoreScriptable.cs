using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TextLore", menuName = "Data/Lore")]
public class LoreScriptable : ScriptableObject
{
	public string Lore;
}
