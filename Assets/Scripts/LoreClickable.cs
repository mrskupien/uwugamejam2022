using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoreClickable : Clickable
{
    public static bool CanClickAgain { get; set; }
    public Action Click;
    [SerializeField] private LoreScriptable scriptable;


    public override bool Equals(object other)
    {
        return base.Equals(other);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override void OnClick()
    {
        Click?.Invoke();
        ShowLore();
    }

    public override string ToString()
    {
        return base.ToString();
    }

    public void ShowLore()
	{
        if(!CanClickAgain)
            return;

        CanClickAgain = false;
        DialogSystem.Instance.DisplayTextCharacterAwaitable(scriptable.Lore, endPauseSeconds: 1.5f);
	}
}

