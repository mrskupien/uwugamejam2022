using Cysharp.Threading.Tasks;
using Game.Menu;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRunnerStateMachine : Singleton<GameRunnerStateMachine>
{
    [SerializeField]
    private List<StateBasic> states = new List<StateBasic>();

    private StateBasic currentSate;
    public StateBasic CurrentSate => currentSate;

    public void Awake()
    {
        SwitchState(StateType.MaineMenu, true).Forget();
    }

    public async UniTask SwitchState(StateType stateType, bool shouldFade)
    {
        if (currentSate != null)
        {
            if (shouldFade)
                await ScreenFade.Instance.ScreenFadeIn(1, null);
            await currentSate.Exit();
        }
        var newState = states.Find((x) => x.Type == stateType);
        currentSate = newState;
        await currentSate.Enter();

        if (shouldFade)
            await ScreenFade.Instance.ScreenFadeOut(null);
    } 

    public StateBasic ReturnState(StateType stateType)
    {
        return states.Find((x) => x.Type == stateType);
    }
}
