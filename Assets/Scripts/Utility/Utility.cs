using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Utility
{
    public static class Mathfs
    {
        private static Vector3 firstLerpPosition;
        private static Vector3 secondLerpPosition;
        public static Vector3 QuadraticBezier(Vector3 A, Vector3 B, Vector3 C, float t)
        {
            firstLerpPosition = Vector3.Lerp(A, B, t);
            secondLerpPosition = Vector3.Lerp(B, C, t);

            return Vector3.Lerp(firstLerpPosition, secondLerpPosition, t);
        }
    }
    public static class Lerp
    {
        public static IEnumerator SmoothFloat(float from, float to, float maxTime, Action<float> OnNewFloat, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            float initial = from;
            while(true)
            {
                yield return null;
                float t = timer / maxTime;
                OnNewFloat?.Invoke(Mathf.Lerp(initial, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if(timer >= maxTime)
                {
                    OnNewFloat?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }
        public static IEnumerator SmoothFloat(float from, float to, float maxTime, LerpX.SmoothType smoothType, Action<float> OnNewFloat, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            float initial = from;
            while(true)
            {
                yield return null;
                float t = timer / maxTime;
                t.Smooth(smoothType);
                OnNewFloat?.Invoke(Mathf.Lerp(initial, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if(timer >= maxTime)
                {
                    OnNewFloat?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }
        public static IEnumerator SmoothVector(Vector3 from, Vector3 to, float maxTime, Action<Vector3> OnNewPosition, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            Vector3 initial = from;
            while(true)
            {
                yield return null;
                float t = timer / maxTime;
                OnNewPosition?.Invoke(Vector3.Lerp(initial, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if(timer >= maxTime)
                {
                    OnNewPosition?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }
        public static IEnumerator SmoothVector(Vector3 from, Vector3 to, float maxTime, LerpX.SmoothType smoothType, Action<Vector3> OnNewPosition, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            Vector3 initial = from;
            while(true)
            {
                yield return null;
                float t = timer / maxTime;
                t.Smooth(smoothType);
                OnNewPosition?.Invoke(Vector3.Lerp(initial, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if(timer >= maxTime)
                {
                    OnNewPosition?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }
        
        public static IEnumerator SmoothVectorSlerp(Vector3 from, Vector3 to, float maxTime, LerpX.SmoothType smoothType, Action<Vector3> OnNewPosition, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            Vector3 initial = from;
            while(true)
            {
                yield return null;
                float t = timer / maxTime;
                t.Smooth(smoothType);
                OnNewPosition?.Invoke(Vector3.Slerp(initial, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if(timer >= maxTime)
                {
                    OnNewPosition?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }

        public static IEnumerator SmoothQuaternion(Vector3 from, Vector3 to, float maxTime, Action<Quaternion> OnNewRotation, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            Quaternion initial = Quaternion.Euler(from);
            while (true)
            {
                yield return null;
                float t = timer / maxTime;
                OnNewRotation?.Invoke(Quaternion.Lerp(initial, Quaternion.Euler(to), t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if (timer >= maxTime)
                {
                    OnNewRotation?.Invoke(Quaternion.Euler(to));
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }
        public static IEnumerator SmoothQuaternion(Vector3 from, Vector3 to, float maxTime, LerpX.SmoothType smoothType, Action<Quaternion> OnNewRotation, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            Quaternion initial = Quaternion.Euler(from);
            while (true)
            {
                yield return null;
                float t = timer / maxTime;
                t.Smooth(smoothType);
                OnNewRotation?.Invoke(Quaternion.Lerp(initial, Quaternion.Euler(to), t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if (timer >= maxTime)
                {
                    OnNewRotation?.Invoke(Quaternion.Euler(to));
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }

        public static IEnumerator SmoothVectorQBezier(Vector3 from, Vector3 mid, Vector3 to, float maxTime, LerpX.SmoothType smoothType, Action<Vector3> OnNewPosition, Action OnEnd = null, bool deltaTimeScaled = false)
        {
            float timer = 0;
            while (true)
            {
                yield return null;
                float t = timer / maxTime;
                t.Smooth(smoothType);
                OnNewPosition?.Invoke(Mathfs.QuadraticBezier(from, mid, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if (timer >= maxTime)
                {
                    OnNewPosition?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }

        public static IEnumerator SmoothVectorQBezier(Vector3 from, Vector3 mid, Vector3 to, float maxTime, Action<Vector3> OnNewPosition, Action OnEnd = null, bool deltaTimeScaled = false)
        {
            float timer = 0;
            while (true)
            {
                yield return null;
                float t = timer / maxTime;
                OnNewPosition?.Invoke(Mathfs.QuadraticBezier(from, mid, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if (timer >= maxTime)
                {
                    OnNewPosition?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }
        public static IEnumerator SmoothQuaternion(Quaternion from, Quaternion to, float maxTime, LerpX.SmoothType smoothType, Action<Quaternion> OnNewRotation, Action OnEnd = null, bool deltaTimeScaled = true)
        {
            float timer = 0;
            while (true)
            {
                yield return null;
                float t = timer / maxTime;
                t.Smooth(smoothType);
                OnNewRotation?.Invoke(Quaternion.Lerp(from, to, t));
                timer += deltaTimeScaled ? Time.deltaTime : Time.unscaledDeltaTime;
                if (timer >= maxTime)
                {
                    OnNewRotation?.Invoke(to);
                    OnEnd?.Invoke();
                    yield break;
                }
            }
        }
    }
}

public static class ExtensionMethods
{
    public static float Sign(this float f) => f >= 0 ? 1 : -1;
    public static void ResetTransformToDefault(this Transform transform, bool inLocalSpace = true)
    {
        if (inLocalSpace)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
        else
        {
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;
        }
    }
}
public static class LerpX
{
    public enum SmoothType { Smoothstep, Smootherstep, EaseOut, EaseIn, PingPongSmooth }
    public static void Smooth(this ref float t, SmoothType interpolate)
    {
        switch(interpolate)
        {
            default:
            case SmoothType.Smoothstep: t = t * t * (3f - 2f * t); break;
            case SmoothType.Smootherstep: t = t * t * t * (t * (6f * t - 15f) + 10f); break;
            case SmoothType.EaseOut: t = 1f - ((1 - t) * (1 - t) * (1 - t)); break;
            case SmoothType.EaseIn: t = t * t * t; break;
            case SmoothType.PingPongSmooth: t = Mathf.Sin(2 * (t - 0.25f) * Mathf.PI) * 0.5f + 0.5f; break;
        }
    }
    public static float GetSmooth(this ref float t, SmoothType interpolate)
    {
        return interpolate switch
        {
            SmoothType.Smoothstep => t * t * (3f - 2f * t),
            SmoothType.Smootherstep => t * t * t * (t * (6f * t - 15f) + 10f),
            SmoothType.EaseOut => 1f - ((1 - t) * (1 - t) * (1 - t)),
            SmoothType.EaseIn => t * t * t,
            SmoothType.PingPongSmooth => Mathf.Sin(2 * (t - 0.25f) * Mathf.PI) * 0.5f + 0.5f,
            _ => t * t * (3f - 2f * t),
        };
    }
}

public static class ListX
{
    public static T Random<T>(this IList<T> list)
	{
        int random = UnityEngine.Random.Range(0, list.Count);
        return list[random];
    }
}