﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class Interactable : MonoBehaviour
{
    public static int InteractableLayer = 6;

    [SerializeField] OutlineHandler[] outlineHandlers;

    public virtual void OnRayEnter()
    {
        foreach (var outlineHandler in outlineHandlers)
        {
            outlineHandler.ChangeOutlineColor(Color.yellow);
            outlineHandler.ToggleOutline(true);
        }
    }
    public virtual void OnRayStay()
    {
        foreach (var outlineHandler in outlineHandlers)
        {
            outlineHandler.ToggleOutline(true);
        }
    }
    public virtual void OnRayExit()
    {
        foreach (var outlineHandler in outlineHandlers)
        {
            outlineHandler?.ToggleOutline(false);
        }
    }

    private void Reset()
    {
        gameObject.layer = 6;
    }
}
