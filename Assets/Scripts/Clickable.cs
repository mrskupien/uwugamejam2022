﻿public abstract class Clickable : Interactable
{
    public abstract void OnClick();
}
