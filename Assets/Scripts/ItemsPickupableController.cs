﻿using System;
using UnityEngine;

public class ItemsPickupableController : MonoBehaviour
{
    Pickupable objectInHands;

    [SerializeField] private PlacementArea playerHandsPlacementArea;
    [SerializeField] private PlacementArea radioCamPlacementArea;
    public bool PlayerHandsFull => objectInHands != null;
    public Pickupable ObjectInHands => objectInHands;

    public PlacementArea RadioCamPlacementArea  => radioCamPlacementArea;
    public void HandlePutDown(bool shouldLerp, PlacementArea placementArea = null)
    {
        if (PlayerHandsFull)
        {
            if (placementArea != null)
            {
                objectInHands.OnPutDown(placementArea, shouldLerp);
            }
            else
            {
                LetObjectInHandsToFall();
            }
            ClearPlayerHands();
        }
    }

    private void LetObjectInHandsToFall()
    {
        objectInHands.transform.SetParent(null);
        objectInHands.ObjRigidbody.isKinematic = false;
    }

    public void HandlePickingUp(Pickupable pickupable, bool shouldLerp)
    {
        if (!PlayerHandsFull)
        {
            pickupable.OnPickUp(RaycastManager.Instance.CurrentCastingType == CastingType.FromCrosshair ? playerHandsPlacementArea : radioCamPlacementArea, shouldLerp);
            objectInHands = pickupable;
            objectInHands.gameObject.layer = Pickupable.PickupableLayer;
        }
    }

    public void ClearPlayerHands()
    {
        objectInHands.gameObject.layer = Interactable.InteractableLayer;
        objectInHands = null;
    }
}
