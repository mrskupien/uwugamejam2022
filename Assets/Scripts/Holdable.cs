﻿public abstract class Holdable : Interactable
{
    public abstract void OnHold();
}
