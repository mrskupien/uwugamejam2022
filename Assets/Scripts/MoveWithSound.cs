using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithSound : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;
    public float updateStep = 0.1f;
    public int sampleDataLength = 1024;

    private float currentUpdateTime = 0f;

    private float clipLoudness;
    private float[] clipSampleData;
    Quaternion targetDirection;
    public float cacehd;

    // Use this for initialization
    void Awake()
    {
        targetDirection = transform.rotation;
        if (!audioSource)
        {
            Debug.LogError(GetType() + ".Awake: there was no audioSource set.");
        }
        clipSampleData = new float[sampleDataLength];

    }

    float timer = 0.0f;
    float timer2 = 0.0f;
    Vector3 rot;
    private bool firstTime = true;

    void Update()
    {

        currentUpdateTime += Time.deltaTime;
        if (currentUpdateTime >= updateStep)
        {
            currentUpdateTime = 0f;
            audioSource.clip.GetData(clipSampleData, audioSource.timeSamples); //I read 1024 samples, which is about 80 ms on a 44khz stereo clip, beginning at the current sample position of the clip.
            clipLoudness = 0f;
            foreach (var sample in clipSampleData)
            {
                clipLoudness += Mathf.Abs(sample);
            }
            clipLoudness /= sampleDataLength; //clipLoudness is what you are looking for
        }
        timer += Time.deltaTime;
        if ((clipLoudness > 0.03 && timer > 2.0f) || firstTime)
        {

            timer = 0.0f;
            timer2 = 0.0f;
            cacehd = clipLoudness;
            rot = new Vector3(0, 0, 1).normalized;
            transform.Rotate(Mathf.Min(firstTime ? 0.3f : clipLoudness, 0.3f) * rot * 50.0f * Time.deltaTime);
            firstTime = false;
        }
        else if (timer < 0.5f)
        {
            transform.Rotate(Mathf.Min(cacehd, 0.3f) * rot * 45.0f * Time.deltaTime * timer);

        }
        else if (timer < 1.0f)
        {
            transform.Rotate(Mathf.Min(cacehd, 0.3f) * rot * 45.0f * Time.deltaTime * (1.0f - timer));

        }
        else
        {
            timer2 += Time.deltaTime * 2.0f;
            transform.rotation = Quaternion.Lerp(transform.rotation, targetDirection, 0.5f * Time.deltaTime * timer2);
        }
    }
}
