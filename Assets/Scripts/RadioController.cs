using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class RadioController : Singleton<RadioController>
{
    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera _cam;
    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera adjust_freq_cam;
    [SerializeField]
    private RadioElementClickable freq_tuning;
    [SerializeField]
    private RadioElementClickable amp_tuning;
    [SerializeField]
    private Material Green;
    [SerializeField]
    private Material RED;
    [SerializeField] GameObject freqCanvas;
    [SerializeField] GameObject radio;
    [SerializeField] GameObject radioVisualHandler;

    [Header("Radio Fizing Stage")]
    [SerializeField] List<PlacementArea> cablePlacementArea;
    [SerializeField] List<PlacementArea> fuzePlacementArea;
    [SerializeField] PlacementArea coverPlacementArea;
    [SerializeField] PlacementArea tablePlacementArea;

    [SerializeField] private List<Pickupable> radioItems;

    [Header("Lores")]
    [SerializeField] LoreScriptable fixBegin;
    [SerializeField] LoreScriptable adjustBegin;

    [Header("Audio")]
    [SerializeField] AudioSource noiseAmp;
    [SerializeField] AudioSource noiseFreq;
    [SerializeField] AudioSource talk;

    bool IsFixDone() => cablePlacementArea.All(x => x.IsOccupied) && fuzePlacementArea.All(x => x.IsOccupied) && coverPlacementArea.IsOccupied;

    private bool focused = false;
    public bool played = false;

    private bool isFixStep = false;
    private bool isAdjustStep = false;
    private bool fixInputEnabled = false;

    private float timerToPlayClick = 0;
    private readonly float timeToPlayNextClick = 0.1f;


    public bool Focused => focused;

    private void OnEnable()
    {
        freqCanvas.SetActive(false);
        ToggleRadioItems(false);
    }

    public void StartInterAction()
    {
        if (!played)
        {
            radio.GetComponent<Collider>().enabled = false;
            if (!isAdjustStep)
            {
                PlayerManager.Instance.TogglePlayer(false);
                RaycastManager.Instance.OnChangeRaycastMode?.Invoke(CastingType.FromMouse);
                adjust_freq_cam.Priority = 0;
                _cam.Priority = 999;
                radioVisualHandler.SetActive(false);
                focused = true;
                isFixStep = true;
                ToggleRadioItems(true);
                fixInputEnabled = true;
                DialogSystem.Instance.DisplayTextCharacterAwaitable(fixBegin.Lore, endPauseSeconds: 1.5f);
            }
            else
            {
                HandleSwitching();
                isFixStep = false;
                isAdjustStep = true;
            }
        }
    }

    public void EndInteraction()
    {
        if (PlayerManager.Instance.ItemsPickupableController.ObjectInHands != null)
        {
            OnPutItemBackOnTable(PlayerManager.Instance.ItemsPickupableController.ObjectInHands);
        }
        focused = false;
        _cam.Priority = 0;
        adjust_freq_cam.Priority = 0;
        RaycastManager.Instance.OnChangeRaycastMode?.Invoke(CastingType.FromCrosshair);
        PlayerManager.Instance.TogglePlayer(true);
        radioVisualHandler.SetActive(true);
        ToggleRadioItems(false);
        isFixStep = false;
        isAdjustStep = false;
        radio.GetComponent<Collider>().enabled = true;
    }

    public void OnPutItemBackOnTable(Pickupable pickupable)
    {
        if (pickupable != null)
        {
            StartCoroutine(PutItemsBackOnTableRoutine(pickupable));
        }
    }

    private IEnumerator PutItemsBackOnTableRoutine(Pickupable pickupable)
    {
        pickupable.OnPutDown(null, true);
        yield return new WaitForSeconds(.6f);
        tablePlacementArea.ReleasePlacementArea();
        PlayerManager.Instance.ItemsPickupableController.ClearPlayerHands();
    }

    void ToggleRadioItems(bool enabled)
    {
        radioItems.ForEach(item => item.gameObject.layer = enabled ? 6 : 0);
    }

    private void Update()
    {
        if (focused)
        {
            if (isFixStep)
            {
                AdjustFixOnUpdate();
                bool fixDone = IsFixDone();
                isFixStep = !fixDone;
                isAdjustStep = fixDone;
                HandleSwitching();
            }
            if (isAdjustStep)
            {
                AdjustFreqOnUpdate();

                if (!played && Mathf.Abs(Green.GetFloat("_Frequency") - RED.GetFloat("_Frequency")) < 0.2f && Mathf.Abs(Green.GetFloat("_Amplitude") - RED.GetFloat("_Amplitude")) < 0.05f)
                {
                    noiseFreq.volume = 0;
                    noiseAmp.volume = 0;
                    talk.volume = 100;
                    played = true;
                    EndInteraction();
                    DoorsController.Instance.Locked = false;
                    TExt().Forget();
                    radio.GetComponent<Collider>().enabled = false;
                    radioItems.ForEach(item => item.ObjCollider.enabled = false);
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EndInteraction();
            }
        }
    }

    private void HandleSwitching()
    {
        if (isAdjustStep)
        {
            noiseAmp.Play();
            noiseFreq.Play();
            talk.Play();
            DialogSystem.Instance.DisplayTextCharacterAwaitable(adjustBegin.Lore, endPauseSeconds: 1.5f);
            ToggleRadioItems(false);
            freqCanvas.SetActive(true);
            fixInputEnabled = false;
            _cam.Priority = 0;
            adjust_freq_cam.Priority = 999;
            var currentEuler = radio.transform.forward;
            var newEuler = Quaternion.AngleAxis(radio.transform.localEulerAngles.y - 180, Vector3.up) * currentEuler;
            StartCoroutine(Utility.Lerp.SmoothVectorSlerp(currentEuler, newEuler, 1f, LerpX.SmoothType.EaseOut,
                (Vector3 newForward) =>
                {
                    radio.transform.localRotation = Quaternion.LookRotation(newForward, Vector3.up);
                }
              )
            );
        }
    }

    void RotateRadio(int way)
    {
        fixInputEnabled = false;
        var currentEuler = radio.transform.forward;
        var newEuler = Quaternion.AngleAxis(180 * way, Vector3.up) * currentEuler;
        StartCoroutine(Utility.Lerp.SmoothVectorSlerp(currentEuler, newEuler, 1f, LerpX.SmoothType.EaseOut,
            (Vector3 newForward) =>
            {
                radio.transform.localRotation = Quaternion.LookRotation(newForward, Vector3.up);
            },
            () => fixInputEnabled = true
            )
        );
    }

    private void AdjustFixOnUpdate()
    {
        if (fixInputEnabled)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                RotateRadio(1);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                RotateRadio(-1);
            }
        }
    }

    private void AdjustFreqOnUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            freq_tuning.transform.Rotate(0, 0, 100.0f * Time.deltaTime);
            TryPlayClick();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            TryPlayClick();
            freq_tuning.transform.Rotate(0, 0, -100.0f * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.W))
        {
            TryPlayClick();
            amp_tuning.transform.Rotate(0, 0, 100.0f * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            TryPlayClick();
            amp_tuning.transform.Rotate(0, 0, -100.0f * Time.deltaTime);
        }
        Debug.LogError(Math.Abs(Green.GetFloat("_Frequency") - RED.GetFloat("_Frequency")));
        Debug.LogError(Math.Abs(Green.GetFloat("_Amplitude") - RED.GetFloat("_Amplitude")));
        noiseFreq.volume = map(Math.Abs(Green.GetFloat("_Frequency") - RED.GetFloat("_Frequency")), 0, 5, 0, 1);
        noiseAmp.volume = Math.Abs(Green.GetFloat("_Amplitude") - RED.GetFloat("_Amplitude"));
        talk.volume = 1.0f - noiseFreq.volume - noiseAmp.volume;

        Green.SetFloat("_Frequency", map(freq_tuning.transform.rotation.eulerAngles.z % 360, 0, 360, 0, 5));
        Green.SetFloat("_Amplitude", map(amp_tuning.transform.rotation.eulerAngles.z % 360, 0, 360, 0, 1));
    }

    public void TryPlayClick()
    {
        timerToPlayClick += Time.deltaTime;
        if (timerToPlayClick > timeToPlayNextClick)
        {
            timerToPlayClick = 0;
            amp_tuning.PlayClick();
        }
    }

    public async UniTask TExt()
    {
        await DialogSystem.Instance.DisplayTextCharacterAwaitable("RADIO: Petliura and Pilsudski welcomed a military parade in the center of Kiev today Ukraine regains its capital", 0, 3f);
        await UniTask.Delay(3000, false);
        await DialogSystem.Instance.DisplayTextCharacterAwaitable("Slava! Let's celebrate", 0, 3f);
    }

    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
}
