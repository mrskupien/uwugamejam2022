using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioClipManager : Singleton<AudioClipManager>
{
    [SerializeField]
    private AudioMixerGroup audioMixerGroupe;


    public enum SoundType
    {
        MainMenuClick = 0,
        Sirens = 1,

        LockedDoor = 2,
        OpenDoor = 3,
        CloseDoor = 4,
        UnlockDoor = 5,

        BagPickup = 10,
        BagPut = 11,
        Bonfire = 12,
        Crispy = 13,
        Bowl_Hit = 14,
        Into_bowl = 15,
        Squeek = 16,

        Radio_Connect_Plastic = 21,
        Radio_Connect_Metal = 22,
        Radio_Click = 23,

        TVBlink = 29,
        TV_Switch_On = 30,
        TV_Switch_Off = 31,
    }

    [Serializable]
    public class AudioStruct
    {
        public SoundType soundType;
        public bool play3D;
        public List<AudioClip> clips = new List<AudioClip>();
    }

    [SerializeField]
    private int Count;

    [SerializeField]
    private List<AudioStruct> audioClips;

    List<AudioSource> audioSources = new List<AudioSource>();

    private void Awake()
    {
        for (int i = 0; i < Count; i++)
        {
            var temp = new GameObject("audio: " + i);
            temp.transform.parent = transform;
            var source = temp.AddComponent<AudioSource>();
            source.outputAudioMixerGroup = audioMixerGroupe;
            source.loop = false;
            audioSources.Add(source);
            temp.SetActive(false);
        }
    }

    public void PlaySound(SoundType soundType, Vector3 pos, float pitchMultiplier = 1f)
    {
        var sound = audioClips.Find(x => x.soundType == soundType);
        if (sound != null)
            for (int i = 0; i < audioSources.Count; i++)
            {
                if (!audioSources[i].gameObject.activeSelf)
                {
                    audioSources[i].clip = sound.clips.Random();
                    audioSources[i].pitch = pitchMultiplier;
                    audioSources[i].gameObject.SetActive(true);
                    audioSources[i].transform.position = pos;
                    audioSources[i].spatialBlend = sound.play3D ? 1.0f : 0.0f;
                    audioSources[i].Play();
                    WaitForPlayEnd(audioSources[i]).Forget();
                    break;
                }
            }
        else
        {
            Debug.LogError("no sound: " + soundType);
        }
    }

    public async UniTask WaitForPlayEnd(AudioSource audioSource)
    {
        await UniTask.Yield();
        await UniTask.WaitUntil(() => audioSource.isPlaying == false);
        audioSource.gameObject.SetActive(false);

    }

    public void ResetSound()
    {
        foreach (var gem in audioSources)
        {
            gem.Stop();
            gem.gameObject.SetActive(false);
        }
    }

}
