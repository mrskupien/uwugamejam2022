using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadPlate : Clickable
{
    [SerializeField] PlacementArea platePlacementArea;
    [SerializeField] private LoreScriptable breadPlacedOnPlate;
    [SerializeField] private LoreScriptable emptyPlateLore;

    public override void OnClick()
    {
        var objinhands = PlayerManager.Instance.ItemsPickupableController.ObjectInHands;
        if (objinhands != null && objinhands.TryGetComponent(out BakeTheBread bakedBread))
        {
            bakedBread.gameObject.layer = 0;
            PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, platePlacementArea);
            DialogSystem.Instance.DisplayTextCharacterAwaitable(breadPlacedOnPlate.Lore, endPauseSeconds: 1.5f).Forget();
            DoorsController.Instance.Locked = false;
            bakedBread.ObjCollider.enabled = false;
        }
		else
		{
            DialogSystem.Instance.DisplayTextCharacterAwaitable(emptyPlateLore.Lore, endPauseSeconds: 1.5f).Forget();
        }
    }
}
