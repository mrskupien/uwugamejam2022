using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : Singleton<MusicManager>
{
    [Serializable]
    public class MusicSets
    {
        public StateType stateType;
        public List<AudioClip> audioClips;
    }

    [SerializeField]
    private List<MusicSets> musicSets;
    [SerializeField]
    private AudioSource audioSource;


    public void PlayeMusic(StateType stateType, float vol = 1.0f)
    {
        audioSource.clip = musicSets.Find(x => x.stateType == stateType).audioClips[0];
        audioSource.volume = vol;
        audioSource.Play();

    }

    public void PlayeMusic(StateType stateType, int ClipNumber)
    {
        audioSource.clip = musicSets.Find(x => x.stateType == stateType).audioClips[ClipNumber];
        audioSource.Play();
    }

    public IEnumerator ClampVolumeDown()
    {
        var cac = audioSource.volume;
        float timer = 0;
        while (timer < 4.0f)
        {
            timer += Time.deltaTime;
            audioSource.volume = cac * (1.0f - timer / 4.0f);
            yield return null;
        }
    }

}
