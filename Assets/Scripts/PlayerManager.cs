﻿using Cinemachine;
using StarterAssets;
using System;
using UnityEngine;

public class PlayerManager : Singleton<PlayerManager>
{
    [SerializeField] GameObject playerObject;
    [SerializeField] GameObject playerCrosshair;
    [SerializeField] Cinemachine.CinemachineVirtualCamera playerCamera;
    [SerializeField] FirstPersonController fpsController;
    [SerializeField] ItemsPickupableController itemsPickupableController;
    public GameObject PlayerObject => playerObject;
    public GameObject PlayerCrosshair => playerCrosshair;
    public CinemachineVirtualCamera PlayerCamera => playerCamera;

    public ItemsPickupableController ItemsPickupableController => itemsPickupableController;

    public void TogglePlayer(bool enabled)
    {
        fpsController.CameraRotationEnabled = enabled;
        fpsController.MovementEnabled = enabled;
        playerObject.SetActive(enabled);
        TogglePlayerCrosshair(enabled);
        TogglePlayerCamera(enabled);
        CursorManager.Instance.CurrentCursorType = enabled ? CursorType.Crosshair : CursorType.UICursor;

    }

    public void TogglePlayerCrosshair(bool enabled) => playerCrosshair.SetActive(enabled);

    public void TogglePlayerCamera(bool enabled) => playerCamera.Priority = enabled ? 999 : 0;
}