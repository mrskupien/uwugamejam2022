using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorClickable : Clickable
{
    public Action Click;

    public override bool Equals(object other)
    {
        return base.Equals(other);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override void OnClick()
    {
        Click?.Invoke();
    }

    public override string ToString()
    {
        return base.ToString();
    }
}
