using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableFireplaceSound : MonoBehaviour
{
	public static System.Action OnEnableFireplaceSound;
	private AudioSource audioSource;

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
		audioSource.enabled = false;
		OnEnableFireplaceSound = EnableSound;
	}
	private void EnableSound()
	{
		audioSource.enabled = true;
		audioSource.Play();
	}
}
