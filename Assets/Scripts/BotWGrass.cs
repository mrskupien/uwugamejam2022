using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotWGrass : MonoBehaviour
{
    [SerializeField] private WindZone windZone;

    private new Renderer renderer;

    private void Awake()
    {
        renderer = GetComponent<Renderer>();
        var bounds = renderer.localBounds;
        bounds.size = bounds.size * 10.0f;
        renderer.localBounds = bounds;
    }

}
