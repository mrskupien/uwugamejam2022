﻿using UnityEngine;

public class GlobusClickable : LoreClickable
{
    [SerializeField] Transform globus;
    private bool canRotate = true;
    public override void OnClick()
    {
        base.OnClick();
        if (!canRotate) return;
        canRotate = false;
        var currentEuler = globus.forward;
        var newEuler = Quaternion.AngleAxis(globus.localEulerAngles.y - 180, Vector3.up) * currentEuler;
        StartCoroutine(Utility.Lerp.SmoothVectorSlerp(currentEuler, newEuler, 1.5f, LerpX.SmoothType.EaseOut,
            (Vector3 newForward) =>
            {
                globus.localRotation = Quaternion.LookRotation(newForward, Vector3.up);
            },
            () => canRotate = true
          )
        ) ;
    }
}
