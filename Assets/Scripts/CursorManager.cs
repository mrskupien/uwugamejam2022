﻿using UnityEngine;

public enum CursorType
{
    None = 0,
    Crosshair = 1,
    UICursor = 2,
}

public class CursorManager : Singleton<CursorManager>
{
    [SerializeField] GameObject playerDot;

    CursorType currentCursorType;

    void LockCursor(bool isLocked)
    { 
        if (isLocked) 
        {
            Cursor.lockState = CursorLockMode.Locked; 
        } 
        else 
        { 
            Cursor.lockState = CursorLockMode.Confined; 
        } 
        Cursor.visible = !isLocked;
    }


    public CursorType CurrentCursorType
    {
        get => currentCursorType;
        set
        {
            currentCursorType = value;
            LockCursor(value == CursorType.Crosshair);
            playerDot.SetActive(value == CursorType.Crosshair);
        }
    }


}
