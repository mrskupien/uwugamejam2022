using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMaidan : StateBasic
{
    public override StateType Type => StateType.Maidan;

    public override async UniTask Enter()
    {
        gameObject.SetActive(true);
        GameRunnerStateMachine.Instance.ReturnState(StateType.Early1900).gameObject.SetActive(false);
        GameRunnerStateMachine.Instance.ReturnState(StateType.Final).gameObject.SetActive(true);
    }

    public override async UniTask Exit()
    {

    }
}
