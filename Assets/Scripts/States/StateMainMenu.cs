using Cinemachine;
using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateMainMenu : StateBasic
{
    [SerializeField]
    private CinemachineVirtualCamera virtualCamera;
    [SerializeField]
    private Button button;
    [SerializeField]
    private GameObject MainMenuGameObject;

    public override StateType Type => StateType.MaineMenu;

    public override async UniTask Enter()
    {
        gameObject.SetActive(true);
        MainMenuGameObject.SetActive(true);
        virtualCamera.Priority = 999;
        CursorManager.Instance.CurrentCursorType = CursorType.UICursor;
        PlayerManager.Instance.TogglePlayer(false);
        MusicManager.Instance.PlayeMusic(StateType.MaineMenu, 0.2f);
        GameRunnerStateMachine.Instance.ReturnState(StateType.Medival).gameObject.SetActive(true);
        TVController.Instance.played = false;
        RadioController.Instance.played = false;
    }

    public override async UniTask Exit()
    {
        MainMenuGameObject.SetActive(false);
        virtualCamera.Priority = 0;
    }

    public void OnEnterGame()
    {
        GameRunnerStateMachine.Instance.SwitchState(StateType.Medival, true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
