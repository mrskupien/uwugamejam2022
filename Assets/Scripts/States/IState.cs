using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState
{
    public StateType Type { get; }
    public UniTask Enter();
    public UniTask Exit();
}
