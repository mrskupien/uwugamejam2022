using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Flags]
public enum StateType
{
    None = 1 << 0,
    MaineMenu = 1 << 1,
    Medival = 1 << 2,
    Early1900 = 1 << 3,
    Maidan = 1 << 4,
    Final = 1 << 5,
}
