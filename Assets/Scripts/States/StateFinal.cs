using Cinemachine;
using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFinal : StateBasic
{
    [SerializeField]
    private CinemachineVirtualCamera virtualCamera;
    [SerializeField]
    private CinemachineSmoothPath smoothPath;

    public override StateType Type => StateType.Final;

    private bool entered = false;

    public override async UniTask Enter()
    {
        entered = true;
        LoreClickable.CanClickAgain = false;
        PlayerManager.Instance.TogglePlayer(false);
        virtualCamera.Priority = 999;
        gameObject.SetActive(true);
        await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.Final._0_ExitText, 0.0f, 3.0f);
        await UniTask.Delay(1000, false);
        AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Sirens, Vector3.zero);
        await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.Final._1_ExitText, 0.0f, 3.0f);
        await UniTask.Delay(2000, false);
        await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.Final._2_ExitText, 0.0f, 3.0f);
        await UniTask.Delay(2000, false);
        await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.Final._3_ExitText, 0.0f, 3.0f);
        await UniTask.Delay(2000, false);
        await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.Final._4_ExitText, 0.0f, 3.0f);
        await UniTask.Delay(2000, false);
        await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.Final._5_ExitText, 0.0f, 3.0f);
        LoreClickable.CanClickAgain = true;
        await MusicManager.Instance.ClampVolumeDown();
        GameRunnerStateMachine.Instance.ReturnState(StateType.Maidan).gameObject.SetActive(false);
        GameRunnerStateMachine.Instance.SwitchState(StateType.MaineMenu, true).Forget();
        AudioClipManager.Instance.ResetSound();
    }

    public override async UniTask Exit()
    {
        entered = false;
        gameObject.SetActive(false);
        virtualCamera.Priority = 0;
        timer = 0;
    }

    float timer = 0;

    public void Update()
    {
        if (entered)
        {
            timer += Time.deltaTime;
            virtualCamera.ForceCameraPosition(smoothPath.EvaluatePosition(timer * 0.019f), smoothPath.EvaluateOrientation(timer * 0.019f));
        }
    }
}
