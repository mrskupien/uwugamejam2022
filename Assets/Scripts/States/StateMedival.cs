using Cinemachine;
using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMedival : StateBasic
{
    public override StateType Type => StateType.Medival;

    public bool oHShit = false;

    public override async UniTask Enter()
    {
        gameObject.SetActive(true);
        if (oHShit)
            await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.RoomMedieval._1_InitText, endPauseSeconds: 1f);
        GameRunnerStateMachine.Instance.ReturnState(StateType.Early1900).gameObject.SetActive(true);
        PlayerManager.Instance.TogglePlayer(true);
        oHShit = true;

        SpawnTEST().Forget();

    }

    public async UniTask SpawnTEST()
    {
        await DialogSystem.Instance.DisplayTextCharacterAwaitable(GlobalText.RoomMedieval._2_InitText, startPauseSeconds: 1f, endPauseSeconds: 1f);
        LoreClickable.CanClickAgain = true;

    }

    public override async UniTask Exit()
    {
    }
}