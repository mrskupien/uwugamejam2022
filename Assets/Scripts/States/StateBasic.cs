using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateBasic : MonoBehaviour, IState
{
    public virtual StateType Type => StateType.None;

    public virtual async UniTask Enter()
    {
    }

    public virtual async UniTask Exit()
    {

    }
}
