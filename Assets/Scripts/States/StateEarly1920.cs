using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateEarly1920 : StateBasic
{
    public override StateType Type => StateType.Early1900;

    public override async UniTask Enter()
    {
        GameRunnerStateMachine.Instance.ReturnState(StateType.Maidan).gameObject.SetActive(true);
        GameRunnerStateMachine.Instance.ReturnState(StateType.Medival).gameObject.SetActive(false);
    }

    public override async UniTask Exit()
    {
    }
}
