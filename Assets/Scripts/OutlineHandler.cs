﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Renderer))]
public class OutlineHandler : MonoBehaviour
{
    [SerializeField] Material outlineMatRef;

    private Renderer objRenderer;
    private LocalKeyword keyWord;
    private List<Material> outlineMatInstance= new List<Material>();
    public bool OutlineEnabled { get; private set; }

    private void Awake()
    {
        objRenderer = GetComponent<Renderer>();

        List<Material> newSharedMaterials = objRenderer.sharedMaterials.ToList();
        foreach (Material material in newSharedMaterials)
        {
            outlineMatInstance.Add(Instantiate(outlineMatRef));
        }
        newSharedMaterials.AddRange(outlineMatInstance);

        objRenderer.sharedMaterials = newSharedMaterials.ToArray();
        keyWord = new LocalKeyword(outlineMatInstance[0].shader, "_ENABLE");
    }


    [ContextMenu("Test Toogle")]
    public void ToggleOutline(bool enabled)
    {
        if (enabled)
        {
            if (OutlineEnabled) return;
            foreach(var test in outlineMatInstance)
            {
                test.EnableKeyword(keyWord);
            }
        }
        else
        {
            foreach (var test in outlineMatInstance)
            {
                test?.DisableKeyword(keyWord);
            }
        }
        OutlineEnabled = enabled;
    }

    public void ChangeOutlineColor(Color color)
    {
        foreach (var test in outlineMatInstance)
        {
            test.SetColor("_Color", color);
        }
    }

    private void Reset()
    {
#if UNITY_EDITOR
        outlineMatRef = AssetDatabase.LoadAssetAtPath<Material>("Assets/Shaders/Outline/Mat_Outline_Normal.mat");
#endif
    }
}
