using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTEXTColor : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI TextMeshProUGUI;
    [SerializeField]
    private Image backgroundImage;
    private Color initColor;

    private void Awake()
    {
        initColor = backgroundImage.color;
    }

    private void OnEnable()
    {
        PointerExit();
    }

    public void PointerOver()
    {
        TextMeshProUGUI.color = new Color(0.85f, 0.85f, 0.85f, 1.0f);
        backgroundImage.color = initColor * 1.3f;
    }

    public void PointerExit()
    {
        TextMeshProUGUI.color = Color.white;
        backgroundImage.color = initColor;
    }

    public void Click()
    {
        TextMeshProUGUI.color = new Color(0.35f, 0.35f, 0.35f, 1.0f);
        AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.MainMenuClick, Vector3.zero);
    }
}
