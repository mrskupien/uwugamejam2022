﻿using System;
using UnityEngine;

public class PlacementArea : MonoBehaviour
{
    public Action OnPickablePlaced;
    [SerializeField] Transform placementTransform;

    public bool IsOccupied { get; set; }

    private Pickupable pickableInArea;
    public Pickupable PickupableInPlacementArea
    {
        get => pickableInArea;
		set
		{
            pickableInArea = value;
            OnPickablePlaced?.Invoke();
        }
    }
    public Transform PlacementTransform => placementTransform;

    public virtual void ReleasePlacementArea()
    {
        PickupableInPlacementArea.ObjRigidbody.isKinematic = false;
        PickupableInPlacementArea.transform.SetParent(placementTransform);
        IsOccupied = false;
        PickupableInPlacementArea = null;
    }

    public virtual void SetObjectOnPlacementTransform(Pickupable obj, bool shouldLerp, bool isForPickup, bool isBread)
    {
        obj.ObjRigidbody.isKinematic = true;
        obj.transform.SetParent(placementTransform);

        if (shouldLerp)
        {
            AnimateToPlacementArea(() =>
            {
                PickupableInPlacementArea = obj;
            }, obj.transform, isForPickup, isBread);
        }
        else
        {
            obj.transform.ResetTransformToDefault();
            PickupableInPlacementArea = obj;
        }
    }

    public void AnimateToPlacementArea(System.Action actionOnEnd, Transform obj, bool isForPickUp, bool isBread = false)
    {
        if (RadioController.Instance.Focused)
        {
            StartCoroutine(Utility.Lerp.SmoothVector(obj.localPosition, Vector3.zero, .5f, LerpX.SmoothType.EaseOut,
                   (Vector3 newPos) =>
                   {
                       obj.localPosition = newPos;
                   },
                   actionOnEnd
               ));
        }
        else
        {
			if(!isBread)
			{
                StartCoroutine(Utility.Lerp.SmoothVectorQBezier(obj.localPosition, new Vector3(0, .5f, .5f), Vector3.zero, .5f, LerpX.SmoothType.EaseOut,
                    (Vector3 newPos) =>
                    {
                        obj.localPosition = newPos;
                    },
                    actionOnEnd
                ));
            }
			else
            {
                if(isForPickUp)
                    StartCoroutine(Utility.Lerp.SmoothVector(obj.localPosition, new Vector3(-0.4f, 0, -0.4f), .5f, LerpX.SmoothType.EaseOut,
                        (Vector3 newPos) =>
                        {
                            obj.localPosition = newPos;
                        },
                        actionOnEnd
                    ));
                else
                    StartCoroutine(Utility.Lerp.SmoothVectorQBezier(obj.localPosition, new Vector3(0, .5f, .5f), Vector3.zero, .5f, LerpX.SmoothType.EaseOut,
                    (Vector3 newPos) =>
                    {
                        obj.localPosition = newPos;
                    },
                    actionOnEnd
                ));
            }
            
        }

        StartCoroutine(Utility.Lerp.SmoothQuaternion(obj.localEulerAngles, Vector3.zero, .5f, LerpX.SmoothType.EaseOut,
            (Quaternion newRot) =>
            {
                obj.localRotation = newRot;
            }
        ));
    }
}
