using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioElementClickable : Clickable
{
    private AudioSource audioSource;
    [SerializeField] AudioClip analogClip;

    private void Awake()
    {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = analogClip;
    }

    public void PlayClick()
    {
        if(audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        audioSource.Play();
    }

    public override void OnClick()
    {

    }
}
