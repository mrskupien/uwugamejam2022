using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilotController : Clickable
{

    public override bool Equals(object other)
    {
        return base.Equals(other);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override void OnClick()
    {
        TVController.Instance.TurnOnTv();
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
