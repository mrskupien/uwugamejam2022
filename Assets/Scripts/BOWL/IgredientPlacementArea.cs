using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgredientPlacementArea : PlacementArea
{
	public override void SetObjectOnPlacementTransform(Pickupable obj, bool shouldLerp, bool isForPickup, bool isBread = false)
	{
		if(PickupableInPlacementArea)
		{
			Destroy(PickupableInPlacementArea.gameObject);
		}

		base.SetObjectOnPlacementTransform(obj, shouldLerp, isForPickup, isBread);
		obj.GetComponent<Collider>().enabled = false;
	}
}
