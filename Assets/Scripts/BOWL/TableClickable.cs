using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableClickable : Clickable
{
	[SerializeField] private PlacementArea grainArea;
	[SerializeField] private PlacementArea waterArea;
	[SerializeField] private PlacementArea saltArea;
	[SerializeField] private PlacementArea bseedArea;

	private void Awake()
	{
		BakeTheBread.OnBreadMade += DeleteCollider;
	}
	private void OnDestroy()
	{
		BakeTheBread.OnBreadMade -= DeleteCollider;
	}

	private void DeleteCollider()
	{
		GetComponent<Collider>().enabled = false;
	}

	public override void OnClick()
	{
		BreadIngridient ingredient = PlayerManager.Instance.ItemsPickupableController.ObjectInHands as BreadIngridient;
        if (ingredient == null) 
			return;

		AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.BagPut, transform.position);
		switch(ingredient.IngridientType)
		{
			case BreadIngridientType.None:
				break;
			case BreadIngridientType.Flour:
				PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, grainArea);
				break;
			case BreadIngridientType.Water:
				PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, waterArea);
				break;
			case BreadIngridientType.Black_Seed:
				PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, bseedArea);
				break;
			case BreadIngridientType.Salt:
				PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, saltArea);
				break;
			default:
				break;
		}
		IngredientCounter.OnPutIngredient?.Invoke(ingredient.IngridientType, ingredient.Weight);
	}
}
