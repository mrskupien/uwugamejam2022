using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeBreadHappen : MonoBehaviour
{
	private SkinnedMeshRenderer m_Renderer;
	private Collider m_Collider;
	[SerializeField] private LoreScriptable breadMade;

	private void Awake()
	{
		m_Renderer = GetComponent<SkinnedMeshRenderer>();
		m_Collider = GetComponent<Collider>();
		m_Collider.enabled = false;
		m_Renderer.enabled = false;
		BowlMakeBreadAction.OnSpawnBread += SpawnBread;
	}
	private void OnDestroy()
	{
		BowlMakeBreadAction.OnSpawnBread -= SpawnBread;
	}

	private void SpawnBread()
	{
		StartCoroutine(WaitForMagic());
		IEnumerator WaitForMagic()
		{
			m_Renderer.enabled = true;
			yield return Utility.Lerp.SmoothFloat(transform.localScale.x, 1, 1.5f, LerpX.SmoothType.Smootherstep,
				(newFloat) => transform.localScale = new Vector3(newFloat, newFloat, newFloat));
			DialogSystem.Instance.DisplayTextCharacterAwaitable(breadMade.Lore, endPauseSeconds: 1.5f).Forget();
			m_Collider.enabled = true;
		}
	}

}
