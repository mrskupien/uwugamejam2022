using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BowlMakeBreadAction : MonoBehaviour
{
	public static Action MakeBowlDoMagic;
	public static Action OnSpawnBread;

	[SerializeField] private Transform bowlTarget;
	[SerializeField] private Transform firstIng;
	[SerializeField] private Transform secondIng;
	[SerializeField] private Transform thirdIng;
	[SerializeField] private Transform fourthIng;
	private Vector3 initialBowlPosition;

	private List<BreadIngridientType> goodIgredients = new List<BreadIngridientType>();

	private void Awake()
	{
		initialBowlPosition = bowlTarget.position + Vector3.up * 0.284f;
		MakeBowlDoMagic = DoMagic;
		IngredientCounter.OnGoodIngredient += AddIgredient;
	}
	private void OnDestroy()
	{
		IngredientCounter.OnGoodIngredient -= AddIgredient;
	}

#if UNITY_EDITOR
	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.B))
			MakeBowlDoMagic?.Invoke();
	}
#endif

	private void AddIgredient(BreadIngridientType addedType)
	{
		if(goodIgredients.Contains(addedType))
			return;

		goodIgredients.Add(addedType);
		if(goodIgredients.Count >= 4)
			MakeBowlDoMagic?.Invoke();
	}

	private void DoMagic()
	{
		StartCoroutine(WaitForMagic());
		IEnumerator WaitForMagic()
		{
			//bring ings to bowl
			float y1Position = 0;
			StartCoroutine(Utility.Lerp.SmoothFloat(firstIng.position.y, firstIng.position.y + 0.65f, 1.2f, LerpX.SmoothType.PingPongSmooth,
				(newFloat) => y1Position = newFloat));
			StartCoroutine(Utility.Lerp.SmoothVector(firstIng.position, bowlTarget.position, 1f,
				(newPosition) =>
				{
					newPosition.y = y1Position;
					firstIng.position = newPosition;
				},
				OnEnd: () => AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Into_bowl, transform.position, 1)
				));

			yield return new WaitForSeconds(0.2f);

			float y2Position = 0;
			StartCoroutine(Utility.Lerp.SmoothFloat(secondIng.position.y, secondIng.position.y + 0.6f, 1.4f, LerpX.SmoothType.PingPongSmooth,
				(newFloat) => y2Position = newFloat));
			StartCoroutine(Utility.Lerp.SmoothVector(secondIng.position, bowlTarget.position, 1.2f,
				(newPosition) =>
				{
					newPosition.y = y2Position;
					secondIng.position = newPosition;
				},
				OnEnd: () => AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Into_bowl, transform.position, 1.05f)
				));

			yield return new WaitForSeconds(0.2f);

			float y3Position = 0;
			StartCoroutine(Utility.Lerp.SmoothFloat(thirdIng.position.y, thirdIng.position.y + 0.55f, 1.6f, LerpX.SmoothType.PingPongSmooth,
				(newFloat) => y3Position = newFloat));
			StartCoroutine(Utility.Lerp.SmoothVector(thirdIng.position, bowlTarget.position, 1.4f,
				(newPosition) =>
				{
					newPosition.y = y3Position;
					thirdIng.position = newPosition;
				},
				OnEnd: () => AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Into_bowl, transform.position, 1.1f)
				));

			yield return new WaitForSeconds(0.2f);

			float y4Position = 0;
			StartCoroutine(Utility.Lerp.SmoothFloat(fourthIng.position.y, fourthIng.position.y + 0.5f, 1.8f, LerpX.SmoothType.PingPongSmooth,
				(newFloat) => y4Position = newFloat));
			yield return Utility.Lerp.SmoothVector(fourthIng.position, bowlTarget.position, 1.6f,
				(newPosition) =>
				{
					newPosition.y = y4Position;
					fourthIng.position = newPosition;
				},
				OnEnd: () => AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Into_bowl, transform.position, 1.15f)
				);

			yield return new WaitForSeconds(0.1f);

			firstIng.gameObject.SetActive(false);
			secondIng.gameObject.SetActive(false);
			thirdIng.gameObject.SetActive(false);
			fourthIng.gameObject.SetActive(false);

			StartCoroutine(Utility.Lerp.SmoothVector(bowlTarget.position, bowlTarget.position + Vector3.up * 1f, 2f, LerpX.SmoothType.Smoothstep,
				(newPosition) => bowlTarget.position = newPosition));

			yield return Utility.Lerp.SmoothFloat(0, 360*3, 3f, LerpX.SmoothType.Smoothstep,
				(newFloat) => bowlTarget.rotation = Quaternion.AngleAxis(newFloat, Vector3.up));

			yield return new WaitForSeconds(0.2f);

			StartCoroutine(Utility.Lerp.SmoothFloat(0, 180, 0.6f, LerpX.SmoothType.Smoothstep,
				(newFloat) => bowlTarget.rotation = Quaternion.AngleAxis(newFloat, Vector3.left)));

			yield return Utility.Lerp.SmoothVector(bowlTarget.position, initialBowlPosition, 1f, LerpX.SmoothType.Smoothstep,
				(newPosition) => bowlTarget.position = newPosition);
			AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Bowl_Hit, transform.position);

			yield return new WaitForSeconds(0.2f);
			OnSpawnBread?.Invoke();
			yield return Utility.Lerp.SmoothVector(bowlTarget.position, bowlTarget.position + Vector3.up * 5, 2f, LerpX.SmoothType.EaseIn,
				(newPosition) => bowlTarget.position = newPosition);
		}
	}
}
