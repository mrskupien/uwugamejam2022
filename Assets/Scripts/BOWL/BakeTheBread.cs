using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BakeTheBread : Pickupable
{
	public static Action StartBakingBread;
	public static Action OnBreadMade;
	[SerializeField] private LoreScriptable breadPickedUpToBeThrownIntoKlin;
	[SerializeField] private LoreScriptable breadBakedFully;
	[SerializeField] private LoreScriptable breadBakedPickedUpOnTheEnd;
	private Material breadMaterial;
	private SkinnedMeshRenderer meshRenderer;
	private bool isBaked;

	private void Awake()
	{
		meshRenderer = GetComponent<SkinnedMeshRenderer>();
		breadMaterial = meshRenderer.material;
		meshRenderer.material = breadMaterial;
		StartBakingBread = DoMagic;
		isBaked = false;
	}

	private void DoMagic()
	{
		StartCoroutine(WaitForMagic());
		IEnumerator WaitForMagic()
		{
			AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Crispy, transform.position);
			float soundTimer = 1f;
			GetComponent<Collider>().enabled = false;
			OnBreadMade?.Invoke();
			yield return Utility.Lerp.SmoothFloat(100, 0, 10, LerpX.SmoothType.Smoothstep,
				(newFloat) =>
				{
					soundTimer -= Time.deltaTime;
					if(soundTimer <= 0)
					{
						AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Crispy, transform.position);
						soundTimer = UnityEngine.Random.Range(1f, 2f);
					}

					meshRenderer.SetBlendShapeWeight(0, newFloat);
					breadMaterial.SetFloat("_change", 1 - (newFloat / 100f));
				});
			isBaked = true;
			GetComponent<Collider>().enabled = true;
			DialogSystem.Instance.DisplayTextCharacterAwaitable(breadBakedFully.Lore, endPauseSeconds: 1.5f);
		}
	}

	public override void OnPickUp(PlacementArea occupiedPlacementArea, bool shouldLerp = false, bool isBread = false)
	{
		base.OnPickUp(occupiedPlacementArea, shouldLerp, true);

		if(!isBaked)
			DialogSystem.Instance.DisplayTextCharacterAwaitable(breadPickedUpToBeThrownIntoKlin.Lore, endPauseSeconds: 1.5f);
		else
		{
			LoreClickable.CanClickAgain = true;
			DialogSystem.Instance.DisplayTextCharacterAwaitable(breadBakedPickedUpOnTheEnd.Lore, endPauseSeconds: 1.5f);
		}
	}
	public override void OnPutDown(PlacementArea placementAreaToOccupy, bool shouldLerp = false, bool isBread = false)
	{
		base.OnPutDown(placementAreaToOccupy, shouldLerp, true);
	}
}
