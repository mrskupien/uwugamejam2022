using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientCounter : MonoBehaviour
{
	public static System.Action<BreadIngridientType, int> OnPutIngredient;
	public static System.Action<BreadIngridientType> OnGoodIngredient;

	[SerializeField] private BreadIngridientType type;
	[SerializeField] private int amountNeeded;
	[SerializeField] private TMPro.TextMeshProUGUI counterText;

	private void Awake()
	{
		OnPutIngredient += PutIngredient;
	}
	private void OnDestroy()
	{
		OnPutIngredient -= PutIngredient;
	}

	private void PutIngredient(BreadIngridientType type, int amount)
	{
		if(type != this.type)
			return;

		int amountSubtract = amountNeeded - amount;
		if(amountSubtract > 0)
		{
			counterText.color = Color.white;
			counterText.text = $"<mspace=0.4em>{amount}/{amountNeeded}</mspace>";

		}
		else if(amountSubtract == 0)
		{
			counterText.color = Color.green;
			counterText.text = $"<mspace=0.4em>{amountNeeded}/{amountNeeded}</mspace>";
			OnGoodIngredient?.Invoke(this.type);
		}
		else
		{
			counterText.color = Color.red;
			counterText.text = $"<mspace=0.4em>{amount}/{amountNeeded}</mspace>";
		}
	}
}
