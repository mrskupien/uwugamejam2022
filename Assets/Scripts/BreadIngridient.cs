using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct BreadIngridientData
{
    public BreadIngridientType BreadIngridientType;
    public int Weight;
}



public enum BreadIngridientType
{
    None = 0,
    Flour = 1,
    Water = 2,
    Black_Seed = 3,
    Salt = 4
}

public class BreadIngridient : Pickupable
{
    [SerializeField] BreadIngridientType ingridientType;

    public BreadIngridientType IngridientType => ingridientType;

    public int Weight { get; set; } = 1;
}
