using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalText
{
	public static class RoomMedieval
	{
		public const string _1_InitText = "Ohh Shit Here We Go Again!!";
		public const string _2_InitText = "I have to make bread for tomorrow, me love some good ol' bread";
	}

	public static class RoomRadio
	{

	}

	public static class Final
	{
		public const string _0_ExitText = "Jokes away";
		public const string _1_ExitText = "Free state of Ukraine was attacked by Russian aggressor";
		public const string _2_ExitText = "This is the sound that is heard daily in whole Ukraine,";
		public const string _3_ExitText = "but they are not alone.";
		public const string _4_ExitText = "Whole world is trying to aid their struggle.";
		public const string _5_ExitText = "Slava to free and independent state of free people of the Ukraine";
	}

}
