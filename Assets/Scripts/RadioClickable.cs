using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioClickable : Clickable
{
    public override void OnClick()
    {
        RadioController.Instance.StartInterAction();
    }
}
