using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScalesInteractable : Clickable
{
    [SerializeField] Transform scalesAnchor;
    [SerializeField] float anglePerOneUnit = 3.5f;
    [SerializeField] TextMeshPro scaleText;
    [SerializeField] PlacementArea ingredientsPlacementArea;
    [SerializeField] PlacementArea weightPlacementArea;
    [SerializeField] PlacementArea switchPlacementArea;
    [SerializeField] Pickupable weighter;
    private int wieghtOnWeightPlacementArea = 1;

    private BreadIngridientType currentBreadIngredientType;

    const int maxUnitsOnSide = 10;

    public void Awake()
    {
        weightPlacementArea.SetObjectOnPlacementTransform(weighter, false, false, false);
        UpdateScale(0);
        UpdateUI(0);
    }

    public override void OnClick()
    {
        if (ingredientsPlacementArea.IsOccupied)
        {
            var objectInHands = PlayerManager.Instance.ItemsPickupableController.ObjectInHands;

            if (objectInHands != null && objectInHands.TryGetComponent(out BreadIngridient breadIngredientToPlace))
            {
                if (currentBreadIngredientType == breadIngredientToPlace.IngridientType)
                {
                    (ingredientsPlacementArea.PickupableInPlacementArea as BreadIngridient).Weight++;
                    AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Squeek, transform.position);

                    breadIngredientToPlace.ObjRigidbody.isKinematic = true;
                    breadIngredientToPlace.transform.SetParent(ingredientsPlacementArea.PlacementTransform);

                    ingredientsPlacementArea.AnimateToPlacementArea(
                        () =>
                        {
                            var newWeight = (ingredientsPlacementArea.PickupableInPlacementArea as BreadIngridient).Weight;
                            UpdateScale(newWeight);
                            UpdateUI(newWeight);
                            Destroy(breadIngredientToPlace.gameObject);
                        }, breadIngredientToPlace.transform, false);
                }
                else
                {
                    StartCoroutine(SwitchIngridientsRoutine());
                }
            }
            else
            {
                PlayerManager.Instance.ItemsPickupableController.HandlePickingUp(ingredientsPlacementArea.PickupableInPlacementArea, true);
                UpdateScale(0);
                UpdateUI(0);
            }
        }
        else
        {
            AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Squeek, transform.position);
            StartCoroutine(PutingDownRoutine());
        }
    }

    private void UpdateUI(int newWeight)
    {
        scaleText.SetText(newWeight.ToString());
    }

    IEnumerator PutingDownRoutine()
    {
        PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, ingredientsPlacementArea);
        yield return new WaitForSeconds(.6f);
        var newIngridient = ingredientsPlacementArea.PickupableInPlacementArea as BreadIngridient;
        currentBreadIngredientType = newIngridient.IngridientType;
        var newWeight = newIngridient.Weight;
        UpdateScale(newWeight);
        UpdateUI(newWeight);
    }

    IEnumerator SwitchIngridientsRoutine()
    {
        if (switchPlacementArea.PickupableInPlacementArea == null)
        { 
            ingredientsPlacementArea.PickupableInPlacementArea.OnPutDown(switchPlacementArea, true);
            yield return new WaitForSeconds(.6f);
            switchPlacementArea.ReleasePlacementArea();
            PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, ingredientsPlacementArea);
            yield return new WaitForSeconds(.6f);
            var newIngridient = ingredientsPlacementArea.PickupableInPlacementArea as BreadIngridient;
            currentBreadIngredientType = newIngridient.IngridientType;
            var newWeight = newIngridient.Weight;
            UpdateScale(newWeight);
            UpdateUI(newWeight);
        }
    }

    void UpdateScale(int newIngredientWeight)
    {
        Vector3 maxRotation = new Vector3(0, 0, maxUnitsOnSide * anglePerOneUnit);

        Vector3 eulerRot = scalesAnchor.transform.localEulerAngles;

        float leftWeightModifier = newIngredientWeight * anglePerOneUnit;
        float rightWeightModifier = wieghtOnWeightPlacementArea * anglePerOneUnit;

        Vector3 leftAnchorRot = new Vector3(0, 0, eulerRot.z + leftWeightModifier);
        Vector3 rightAnchorRot = new Vector3(0, 0, eulerRot.z + rightWeightModifier);

        Vector3 newEulerRot = leftAnchorRot - rightAnchorRot;

        if (Mathf.Abs(newEulerRot.z) >= maxUnitsOnSide * anglePerOneUnit)
        {
            newEulerRot = maxRotation;
        }

        StartCoroutine
        (
            Utility.Lerp.SmoothQuaternion(eulerRot, newEulerRot, .5f, LerpX.SmoothType.EaseOut, (newQuaternion) => scalesAnchor.transform.localRotation = newQuaternion)
        );
    }
}
