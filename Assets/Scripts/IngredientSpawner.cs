using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientSpawner : Clickable
{
    [SerializeField] private BreadIngridient breadIngridient;
    [SerializeField] private Transform spawnedTransform;

    public override void OnClick()
    {
        if (!PlayerManager.Instance.ItemsPickupableController.PlayerHandsFull)
        {
            BreadIngridient spawned = SpawnIngredient();
            AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.BagPickup, transform.position);
            PlayerManager.Instance.ItemsPickupableController.HandlePickingUp(spawned, true);
        }
    }

    BreadIngridient SpawnIngredient()
    {
        var spawnedGO = Instantiate(breadIngridient);
        spawnedGO.transform.SetParent(spawnedTransform);
        spawnedGO.transform.localPosition = Vector3.zero;
        return spawnedGO.GetComponent<BreadIngridient>();
    }
}

