using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class DialogSystem : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textChar;
    [SerializeField]
    private TextMeshProUGUI textSur;
    [SerializeField]
    private GameObject textCharPar;
    [SerializeField]
    private CanvasGroup textCharParCanvasGroup;
    [SerializeField]
    private GameObject textCharSur;
    [SerializeField]
    private RectTransform textCharRectChar;
    [SerializeField]
    private RectTransform textCharRectSur;

    public static DialogSystem Instance;
    private bool diplaying;

    public void Awake()
    {
        Instance = this;
    }
    public async UniTask DisplayTextCharacterAwaitable(string text, float startPauseSeconds = 0, float endPauseSeconds = 0)
    {
        await UniTask.WaitUntil(() => diplaying == false);
        diplaying = true;
        LoreClickable.CanClickAgain = false;
        await WaitForSeconds(startPauseSeconds);
        await StartDisplaying(textCharParCanvasGroup, textCharRectChar, textChar, text, endPauseSeconds);
        LoreClickable.CanClickAgain = true;
        diplaying = false;
    }

    private async UniTask WaitForSeconds(float startPauseSeconds)
    {
        if (startPauseSeconds != 0)
            await Task.Delay((int)startPauseSeconds * 1000);
    }

    public void DisplayTextCharacter(string text)
    {
        StartCoroutine(StartDisplaying(textCharPar, textCharRectChar, textChar, text, true));
    }

    public void DisplayTextSur(string text, Vector3 position)
    {
        textCharSur.transform.position = position;
        StartCoroutine(StartDisplaying(textCharSur, textCharRectSur, textSur, text, true));
    }

    private IEnumerator StartDisplaying(GameObject gameObject, RectTransform rectTransform, TextMeshProUGUI textPro, string text, bool wait)
    {
        textPro.fontSize = Screen.width / 25;
        var test = textPro.GetPreferredValues(text) + new Vector2(110, 0);
        rectTransform.sizeDelta = new Vector2(Math.Min(Screen.width, test.x), test.x < Screen.width ? test.y : test.y * Mathf.CeilToInt(test.x / Screen.width));
        gameObject.SetActive(true);
        int Count = text.Length;
        float time = 0.08f * Count;
        float timer = 0;
        int Counter = 1;
        while (timer < time)
        {
            if (timer > Counter * 0.08f)
            {
                Counter++;
            }
            textPro.text = text.Substring(0, Math.Min(Counter, text.Length));
            timer += Time.unscaledDeltaTime;
            yield return null;
        }
        if (wait)
            yield return new WaitForSeconds(text.Length * .3f);
        gameObject.SetActive(false);
    }

    private IEnumerator StartDisplaying(CanvasGroup canvas, RectTransform rectTransform, TextMeshProUGUI textPro, string text, float pauseSeconds)
    {
        textPro.fontSize = Screen.width / 25;
        var test = textPro.GetPreferredValues(text) + new Vector2(110, 0);
        textPro.text = string.Empty;
        rectTransform.sizeDelta = new Vector2(Math.Min(Screen.width, test.x), test.x < Screen.width ? test.y : test.y * Mathf.CeilToInt(test.x / Screen.width));
        canvas.gameObject.SetActive(true);
        yield return Utility.Lerp.SmoothFloat(0, 1, 0.5f, (newFloat) => canvas.alpha = newFloat);
        int Count = text.Length;
        float time = 0.08f * Count;
        float timer = 0;
        int Counter = 1;
        while (timer < time)
        {
            if (timer > Counter * 0.08f)
            {
                Counter++;
            }
            textPro.text = text.Substring(0, Math.Min(Counter, text.Length));
            timer += Time.unscaledDeltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(pauseSeconds);
        yield return Utility.Lerp.SmoothFloat(1, 0, 0.5f, (newFloat) => canvas.alpha = newFloat);
        canvas.gameObject.SetActive(false);
    }

}
