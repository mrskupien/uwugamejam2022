﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ScreenFadePass : ScriptableRenderPass
{
    ProfilingSampler m_ProfilingSampler = new ProfilingSampler("Fade");
    RenderTargetIdentifier m_CameraColorTarget;
    private FadeSettings settings = null;

    public ScreenFadePass(FadeSettings newSettings)
    {
        settings = newSettings;
        renderPassEvent = newSettings.renderPassEvent;
    }
    public void SetTarget(RenderTargetIdentifier colorHandle)
    {
        m_CameraColorTarget = colorHandle;
    }


    public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
    {
        ConfigureTarget(new RenderTargetIdentifier(m_CameraColorTarget, 0, CubemapFace.Unknown, -1));
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        CommandBuffer command = CommandBufferPool.Get(settings.profilerTag);

        using (new ProfilingScope(command, m_ProfilingSampler))
        {
            command.SetRenderTarget(new RenderTargetIdentifier(m_CameraColorTarget, 0, CubemapFace.Unknown, -1));
            command.DrawMesh(RenderingUtils.fullscreenMesh, Matrix4x4.identity, settings.runTimeMaterial);
        }
        context.ExecuteCommandBuffer(command);

        CommandBufferPool.Release(command);
    }
}