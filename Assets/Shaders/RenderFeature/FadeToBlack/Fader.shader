﻿Shader "Custom/Fader"
{
	Properties
	{
		_FadeColor("FadeColor", Color) = (1, 1, 1)
		_Alpha("Alpha", float) = 0.0
	}

		SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline"}
		ZWrite Off Cull Off ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"


			struct appdata
			{
				float4 positionHCS   : POSITION;
				float2 uv           : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4  positionCS  : SV_POSITION;
				float2  uv          : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			TEXTURE2D_X(_CameraOpaqueTexture);
			SAMPLER(sampler_CameraOpaqueTexture);
			float _Alpha;
			float4 _FadeColor;

			v2f vert(appdata v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				if (_Alpha > 0.01) 
				{
					o.positionCS = float4(v.positionHCS.xyz, 1.0);
				}
				else
				{
					o.positionCS = float4(0,0,0,0);
				}
				

				o.uv = v.uv;
				return o;
			}


			float4 frag(v2f i) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				return _FadeColor * float4(1, 1, 1, _Alpha);
			}
			ENDHLSL
		}
	}
}
