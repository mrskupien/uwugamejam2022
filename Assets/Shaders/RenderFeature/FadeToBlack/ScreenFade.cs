using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Game.Menu
{
    public class ScreenFade : Singleton<ScreenFade>
    {
        [SerializeField] private UniversalRendererData forwardRendererData;
        [SerializeField] private float duration;
        [SerializeField, Range(0, 1)] private float alphaOnStart = 1f;
        [SerializeField]
        private Material fadeMaterial;
        private float currentAlpha;

        private void Awake()
        {
            SetupFadeFeature();
        }

        private void SetupFadeFeature()
        {
            ScriptableRendererFeature feature = forwardRendererData.rendererFeatures.Find(x => x is ScreenFadeFeature);

            if (feature is ScreenFadeFeature screenFade)
            {
                fadeMaterial = Material.Instantiate(screenFade.settings.material);
                screenFade.settings.runTimeMaterial = fadeMaterial;
                fadeMaterial.SetFloat("_Alpha", alphaOnStart);
                currentAlpha = alphaOnStart;
            }
        }

        public async UniTask ScreenFadeOut(Action OnFinish)
        {
            await Utility.Lerp.SmoothFloat(
                currentAlpha, 0, duration,
                (newFloat) => fadeMaterial.SetFloat("_Alpha", newFloat),
                OnFinish);
        }
        public async UniTask ScreenFadeIn(float targetAlpha, Action OnFinish)
        {
            await Utility.Lerp.SmoothFloat(
                 0, targetAlpha, duration,
                 (newFloat) => fadeMaterial.SetFloat("_Alpha", newFloat),
                 OnFinish);
            currentAlpha = targetAlpha;
        }


        [Serializable]
        public class Settings
        {
            public UniversalRendererData forwardRendererData;
            public float duration = 0.5f;
        }
    }
}