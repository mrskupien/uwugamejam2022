using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CablePin : Clickable
{
    [SerializeField] CableType cableTypeForThisPin;
    [SerializeField] PlacementArea cablePlacementArea;
    [SerializeField]
    private SphereCollider cableCollider;

    private void OnEnable()
    {
        cableCollider.enabled = true;
    }

    public override void OnClick()
    {
        if (!cablePlacementArea.IsOccupied)
        {
            var objInHands = PlayerManager.Instance.ItemsPickupableController.ObjectInHands;
            if (objInHands != null && objInHands.TryGetComponent(out RadioCable cable))
            {
                if (cable.CableType == cableTypeForThisPin)
                {
                    PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, cablePlacementArea);
                }
            }
        }
        else
        {
            if (!PlayerManager.Instance.ItemsPickupableController.PlayerHandsFull)
            {
                PlayerManager.Instance.ItemsPickupableController.HandlePickingUp(cablePlacementArea.PickupableInPlacementArea, true);
            }
        }
    }
}
