using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuzePin : Clickable
{
    [SerializeField] PlacementArea fuzePlacementArea;

    [SerializeField]
    private SphereCollider cableCollider;

    private void OnEnable()
    {
        cableCollider.enabled = true;
    }


    public override void OnClick()
    {
        if (!fuzePlacementArea.IsOccupied)
        {
            var objInHands = PlayerManager.Instance.ItemsPickupableController.ObjectInHands;

            if (objInHands != null && objInHands.TryGetComponent(out Fuze fuze))
            {
                PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, fuzePlacementArea);
            }
        }
        else
        {
            if (!PlayerManager.Instance.ItemsPickupableController.PlayerHandsFull)
            {
                PlayerManager.Instance.ItemsPickupableController.HandlePickingUp(fuzePlacementArea.PickupableInPlacementArea, true);
            }
        }
    }
}
