using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CableType
{
    None = 0,
    Red = 1,
    Blue = 2,
    Green = 3
}

public class RadioCable : Pickupable
{
    [SerializeField] CableType cableType;

    public CableType CableType => cableType;
    [SerializeField]
    public PlacementArea placementArea;

    private void OnEnable()
    {
        OnPutDown(placementArea);
    }

    public override void OnPickUp(PlacementArea occupiedPlacementArea, bool shouldLerp = false, bool isBread = false)
    {
        if (CurrentPlacementArea != null)
        {
            AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Radio_Connect_Plastic, CurrentPlacementArea.PlacementTransform.position);
        }
        base.OnPickUp(occupiedPlacementArea, shouldLerp);
    }

    public override void OnPutDown(PlacementArea placementAreaToOccupy, bool shouldLerp = false, bool isBread = false)
    {
        if (placementAreaToOccupy == null)
        {
            placementAreaToOccupy = placementArea;
        }
        base.OnPutDown(placementAreaToOccupy, shouldLerp);
        AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Radio_Connect_Plastic, placementAreaToOccupy.PlacementTransform.position);
        if (placementAreaToOccupy != placementArea)
        {
            GetComponent<BoxCollider>().enabled = false;
            placementAreaToOccupy.transform.GetChild(0).GetComponent<SphereCollider>().enabled = false;
            placementAreaToOccupy.transform.GetChild(1).GetComponent<SphereCollider>().enabled = false;
        }
    }
}
