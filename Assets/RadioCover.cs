using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioCover : Pickupable
{

    [SerializeField]
    public PlacementArea placementArea;


    private void OnEnable()
    {
        GetComponent<BoxCollider>().enabled = true;
        OnPutDown(placementArea);
    }

    public override void OnPutDown(PlacementArea placementAreaToOccupy, bool shouldLerp = false, bool dupa = false)
    {
        if (placementAreaToOccupy == null)
        {
            placementAreaToOccupy = placementArea;
        }
        base.OnPutDown(placementAreaToOccupy, shouldLerp, dupa);
        if (placementAreaToOccupy!= placementArea)
        {
            GetComponent<BoxCollider>().enabled = false;
            placementAreaToOccupy.transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
        }
    }
}
