using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioCoverPlacementPoint : Clickable
{
    [SerializeField]
    private BoxCollider cableCollider;

    private void OnEnable()
    {
        cableCollider.enabled = true;
    }



    [SerializeField] PlacementArea coverPlacementArea;
    public override void OnClick()
    {
        if (!coverPlacementArea.IsOccupied)
        {
            var objInHands = PlayerManager.Instance.ItemsPickupableController.ObjectInHands;

            if (objInHands!= null && objInHands.TryGetComponent(out RadioCover radioCover))
            {
                PlayerManager.Instance.ItemsPickupableController.HandlePutDown(true, coverPlacementArea);
            }
        }
        else
        {
            if (!PlayerManager.Instance.ItemsPickupableController.PlayerHandsFull)
            {
                PlayerManager.Instance.ItemsPickupableController.HandlePickingUp(coverPlacementArea.PickupableInPlacementArea, true);
            }
        }
    }
}
