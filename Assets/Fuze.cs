﻿using UnityEngine;

public class Fuze : Pickupable
{
    [SerializeField]
    public PlacementArea placementArea;

    private void OnEnable()
    {
        GetComponent<SphereCollider>().enabled = true;
        OnPutDown(placementArea);
    }

    public override void OnPickUp(PlacementArea occupiedPlacementArea, bool shouldLerp = false ,bool isBread = false)
    {
        if (CurrentPlacementArea != null)
        {
            AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Radio_Connect_Metal, CurrentPlacementArea.PlacementTransform.position);
        }
        base.OnPickUp(occupiedPlacementArea, shouldLerp, isBread);
    }

    public override void OnPutDown(PlacementArea placementAreaToOccupy, bool shouldLerp = false, bool isBread = false)
    {
        if(placementAreaToOccupy== null)
        {
            placementAreaToOccupy = placementArea;
        }
        base.OnPutDown(placementAreaToOccupy, shouldLerp);
        AudioClipManager.Instance.PlaySound(AudioClipManager.SoundType.Radio_Connect_Metal, placementAreaToOccupy.PlacementTransform.position);
        if ((placementAreaToOccupy != placementArea))
        {
            GetComponent<SphereCollider>().enabled = false;
            placementAreaToOccupy.transform.GetChild(0).GetComponent<SphereCollider>().enabled = false;
        }
    }
}
